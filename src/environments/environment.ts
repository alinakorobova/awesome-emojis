export const environment: { production: boolean, baseUrl: string } = {
    production: false,
    baseUrl: 'https://api.github.com',
};
