import {Component, ViewChild} from '@angular/core';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Emoji, EmojisService} from "../utils/http/emojis.service";
import {LocalstorageService} from "../localstorage.service";

@Component({
  selector: 'table-emojis',
  styleUrls: ['table-emojis.scss'],
  templateUrl: 'table-emojis.html',
})
export class TableOverview {
  displayedColumns = ['name', 'icon', 'action'];
  dataSource: MatTableDataSource<Emoji>;

  emojis: Emoji[] = [];
  favorites: Emoji[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private emojisService: EmojisService, private localstorageService: LocalstorageService) {

  }
  async getEmojis(): Promise<Array<Emoji>> {
    return new Promise((resolve, reject) => {
      this.emojisService.getAllEmojis().subscribe((res: Emoji[]) => {
        resolve(res);
      })
    })
  }
  async ngOnInit(): Promise<void> {
    this.localstorageService.favorites$.subscribe(f => this.favorites = f);
    this.emojisService.filter$.subscribe(f => {
      this.dataSource.filter = f;
    });
    this.emojis = await this.getEmojis();
    this.dataSource = new MatTableDataSource(this.emojis);
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    this.localstorageService.updateFavorites();
  }
  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  }
  addEmoji(em:Emoji) {
    this.localstorageService.updateFavorites(em);
  }
  isFavorite(em:Emoji): boolean {
    return this.favorites.map(f => f.name).indexOf(em.name) >= 0
  }
}