import { Component } from '@angular/core';
import {MatListItem, MatNavList} from "@angular/material/list";
import {RouterLink, RouterLinkActive} from "@angular/router";
import {MatIcon} from "@angular/material/icon";
import {MatLine} from "@angular/material/core";
import {Emoji, EmojisService} from "../utils/http/emojis.service";
import {LocalstorageService} from "../localstorage.service";
import {NgForOf, NgIf} from "@angular/common";
import {MatSidenav, MatSidenavContainer} from "@angular/material/sidenav";

@Component({
  selector: 'left-panel',
  standalone: true,
  imports: [
    MatNavList,
    MatListItem,
    RouterLink,
    RouterLinkActive,
    MatIcon,
    MatLine,
    NgForOf,
    MatSidenavContainer,
    MatSidenav,
    NgIf
  ],
  templateUrl: './left-panel.component.html',
  styleUrl: './left-panel.component.scss'
})
export class LeftPanelComponent {

  favorites: Emoji[] = [];
  constructor(private localstorageService: LocalstorageService) {}
  ngOnInit() {
    this.localstorageService.favorites$.subscribe(f => {
      this.favorites = f
    });
  }

  removeEmoji(em:Emoji) {
    this.localstorageService.updateFavorites(em);
  }

}
