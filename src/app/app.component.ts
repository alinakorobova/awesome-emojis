import { Component } from '@angular/core';
import {Emoji} from "./utils/http/emojis.service";
import {LocalstorageService} from "./localstorage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  favorites: Emoji[] = [];
  constructor(private localstorageService: LocalstorageService) {}
  ngOnInit() {
    this.localstorageService.favorites$.subscribe(f => {
      this.favorites = f
    });
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/