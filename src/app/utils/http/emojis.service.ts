import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import { map } from "rxjs/operators";
import { DataService } from "./data.service";

export interface Emoji {
    name: string,
    path: string,
}
@Injectable({
  providedIn: 'root'
})

export class EmojisService {

  constructor(private data: DataService) {}

  private filter: Subject<any> = new Subject<string>();
  filter$: Observable<any> = this.filter.asObservable();
  getAllEmojis(): Observable<any[]> {
    return this.data
        .getAll<any>(`emojis`)
        .pipe(map(ret => {
            const result: Emoji[] = Object.keys(ret).map((name: string) => ({
                name, path: ret[name]
            }))
            return result
        }));
  }
    updateFilter(f: string = ""): void {
        this.filter.next(f);
    }
}
