import { Component } from '@angular/core';
import {Emoji, EmojisService} from "../utils/http/emojis.service";
import {LocalstorageService} from "../localstorage.service";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {
  emojis: Emoji[] = [];
  constructor(private emojisService: EmojisService, private localstorageService: LocalstorageService) {}
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.emojisService.updateFilter(filterValue.trim().toLowerCase());
  }
}

