import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Emoji} from "./utils/http/emojis.service";

@Injectable({
  providedIn: 'root'
})

export class LocalstorageService {
  private favoritesSubject: Subject<any> = new Subject<Emoji[]>();
  favorites$: Observable<any> = this.favoritesSubject.asObservable();

  favorites: Emoji[] = this.get("favorites") || [];
  constructor() {}


  set(key: string, data: any): void {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {

    }
  }

  get(key: string) {
    try {
      // @ts-ignore
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      return null;
    }
  }
  updateFavorites(element: Emoji | undefined = undefined): void {
    if (element) {
      if (this.favorites.filter(el => el.name === element?.name).length > 0) {
        this.favorites = this.favorites.filter(el => el.name !== element?.name);
      } else {
        this.favorites = [...this.favorites, element];
      }
    }
    this.set("favorites", this.favorites);
    this.favoritesSubject.next(this.favorites);

  }
}